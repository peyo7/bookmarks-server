const bookmarks = [
  {
    id: 1,
    title: "Test one",
    url: "https://www.thinkful.com",
    description: "Test description",
    rating: "5"
  }
];

module.exports = { bookmarks }; 